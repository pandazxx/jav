#!/usr/bin/env node

'use strict';
var vo = require('vo');
var Nightmare = require('nightmare');
var cheerio = require('cheerio');
var request = require('superagent');
var async = require('async');
var colors = require('colors');
var program = require('commander');
var ProgressBar = require('progress');
var userHome = require('user-home');
var path = require('path');
var fs = require('fs');
var Redis = require('ioredis')

var noop = function noop() {};

// global var
var baseUrl = 'http://www.javbus.in';
//const startUrl = 'http://www.javbus.in/series/3hf';
//const startUrl = 'http://www.javbus.in/series/2x5';
var startUrl = 'http://www.javbus.in/series/2b5';
var pageIndex = 1;
var currentPageHtml = null;

function parseIPAddr(val) {
    var pair = val.split(":")
    var port = parseInt(pair.pop())
    var ip = pair.pop() || ""
    return [ip, port]
}

program
	.version('0.1.0')
	.usage('[options]')
	.option('-w, --worker <num>', '设置抓取并发连接数，默认值：2', parseInt, 2)
	.option('-t, --timeout <num>', '自定义连接超时时间(毫秒)。默认值：10000', parseInt, 10000)
	.option('-r,  --redis-server <string>', '设置抓取影片的数量上限，0为抓取全部影片。默认值：0', parseIPAddr, ["redis", "6379"])
	.parse(process.argv);

var workerCnt = 2;
var timeout = 10000;

var redisServer = process.env.redis_ip || 'redis'
var redisPort = process.env.redis_port || 6379

console.log({
    workerCnt: workerCnt,
    timeout: timeout,
    redisServer: redisServer,

});

var redis = new Redis(redisPort, redisServer)


console.log('========== 获取资源站点：%s =========='.green.bold, baseUrl);
console.log('并行连接数：'.green, workerCnt.toString().green.bold, '      ', '连接超时设置：'.green, (timeout / 1000.0).toString().green.bold,'秒'.green);
console.log('磁链保存位置: %s:%d'.green, redisServer[0], redisServer[1]);

var movie_job_queue = async.queue(function(task, callback) {
                                        console.log("Getting magnet from %s".red, task);
                                        getMagnet(task, callback)
                                    },
                                    10)

var index_page_queue = async.queue(function(task, callback) {
    async.retry(3,
               function(callback, result) {
                   console.log("loading %s".green, task);
                   request.get(task)
                          .accept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
                          .set('Accept-Encoding', 'gzip, deflate')
                          .set('Connection', 'keep-alive')
                          .timeout(timeout)
                          .redirects(4)
                          .end(function(err, res) {
                              if (err) {
                                  return callback(err)
                              }
                              let $ = cheerio.load(res.text)
                              let nextUrl = $("a").filter(function(index) {return $(this).text() === '下一頁'}).attr('href')
                              if (!nextUrl) {
                                  console.log("Cannot get next url for page %s".red.bold, task);
                                  return callback()
                              }
                              if (!nextUrl.toUpperCase().startsWith('HTTP')) {
                                  nextUrl = baseUrl + nextUrl
                              }
                              console.log("Found next page %s".green, nextUrl);
                              index_page_queue.push(nextUrl)
                              parseLinks($)
                              return callback(null, res)
                          })
               },
               function(err, res) {
                   if (err) {
                       return callback(err)
                   }
                   return callback()
               })
}, 5)


function parseLinks(c) {
    c('a.movie-box').each(function(i, elem) {
        var movieUrl = c(this).attr('href')
        if (!movieUrl.toUpperCase().startsWith("HTTP")) {
            movieUrl = baseUrl + movieUrl
        }
        console.log("Found movie url: %s".green, movieUrl);
        let movie_id = movieUrl.split("/").pop()
        redis.exists("movie:" + movie_id + ":id").then(function(result) {
            if (result == 1) {
                console.log("Passing known movie %s".gray, movie_id);
            } else {
                movie_job_queue.push(movieUrl)
            }
        })
    })
    c('a.avatar-box').each(function(i, elem) {
        var artressUrl = c(this).attr('href')
        if (!artressUrl.toUpperCase().startsWith('HTTP')) {
            artressUrl = baseUrl + artressUrl
        }
        console.log('Found artress: %s'.green, artressUrl);
        index_page_queue.push(artressUrl)
    })
}
function saveObject(basekey, obj) {
    var pipeline = redis.pipeline()
    for (var prop in obj) {
        let key = basekey + ":" + prop
        let value = obj[prop]
        if (Array.isArray(value)) {
            for (var i = 0; i < value.length; i++) {
                pipeline.sadd(key, value[i])
            }
        } else if (typeof(value) === "object") {
            for (var subkey in value) {
                pipeline.set(key + ":" + subkey, value[subkey])
            }
        } else {
            pipeline.set(key, value)
        }
    }
    pipeline.exec(function(err, result) {
        if (err) {
            console.log("Error in saving obj %s: %s".red, basekey, err);
            throw err
        }
        result.forEach(function(item) {
            if (item[0]) {
                console.log("Error in saving obj %s: %s".red, basekey, item[0]);
                throw item[0]
            }
        })
    })
}

var EXTRA_INFO_KEY_MAPPING = {
    '發行日期': 'ReleaseDate',
    '長度': 'Length',
}

function getMagnet(link, callback) {
    request
        .get(link)
        .timeout(timeout)
        .end(function(err, res) {
            console.log("处理链接: %s id: %s".green, link, link.split('/').pop())
            if (err) {
                console.error('id%s页面获取失败：%s'.red, link.split('/').pop(), err.message);
                return callback(null);
            }
            let $ = cheerio.load(res.text);
            let script = $('script', 'body').eq(2).html();
            if (!script) {
                console.error('id%s页面获取失败：%s'.red, link.split('/').pop(), err.message);
                return callback(null);
            }
            let meta = parse(script);
            var movieInfo = {}
            var artists = []
            $('div.star-box.star-box-common.star-box-up.idol-box > li > a').each(function() {
                let id = $(this).attr("href").split("/").pop()
                let url = $(this).attr("href")
                let avatarUrl = $(this).children('img').first().attr("src")
                let name = $(this).children('img').first().attr("title")
                let artist = {id: id, avatarUrl: avatarUrl, name: name, url: url}
                redis.exists("artist:" + id + ":id").then(function(result) {
                    if (result == 0) {
                        saveObject("artist:" + id, artist)
                        redis.sadd("artists", id)
                    }
                })
                artists.push(id)
            })
            var title = $('h3').text().trim()
            var sensored = $('ul.navbar-nav > li.active > a').text()
            var cover_img = $('div.col-md-9.screencap > a.bigImage > img').attr('src')
            var series = $('span.header').filter(function(index) {return $(this).text().trim() === "系列:"}).siblings('a').text().trim()
            var genre = []
            $('div.col-md-3.info > p > span.genre >a[href*="genre"]').each(function() {genre.push($(this).text().trim())})
            var extraInfo = {}
            $('div.col-md-3.info > p > span.header').each(function() {
                var info = $(this).parent().text().split(": ")
                if (info.length == 2) {
                    var value = info.pop()
                    var key = info.pop()
                    let newkey = EXTRA_INFO_KEY_MAPPING[key]
                    if (newkey) {
                        extraInfo[newkey] = value
                    } else {
                        extraInfo[key] = value
                    }
                }
            })
            console.log("Movie info %s: %s".green, link.split('/').pop(), [title, sensored, cover_img, series, genre].join());
            //console.log('fetch link: %S'.blue, link);
            console.log("getting url %s".red, baseUrl + "/ajax/uncledatoolsbyajax.php?gid=" + meta.gid + "&lang=" + meta.lang + "&img=" + meta.img + "&uc=" + meta.uc + "&floor=" + Math.floor(Math.random() * 1e3 + 1));
            request
                .get(baseUrl + "/ajax/uncledatoolsbyajax.php?gid=" + meta.gid + "&lang=" + meta.lang + "&img=" + meta.img + "&uc=" + meta.uc + "&floor=" + Math.floor(Math.random() * 1e3 + 1))
                .set('Referer', 'http://www.javbus.in/SCOP-094')
                .timeout(timeout)
                .end(function(err, res) {
                    debugger;
                    if (err) {
                        console.error('番号%s磁链获取失败: %s'.red, link.split('/').pop(), err.message);
                        return callback(null); // one magnet fetch fail, do not crash the whole task.
                    };
                    let $ = cheerio.load(res.text);
                    //let anchor = $('[onclick]').first().attr('onclick');
                    let magnets = []
                    $("tr  td:nth-child(2) a").each(function() {
                        //console.log("Magnet: %s (%s)", $(this).attr("href"), $(this).text().trim());
                        magnets.push(JSON.stringify({url: $(this).attr('href'),
                                                     size: $(this).text().trim(),
                                                     date: $(this).parent().next().children().first().text().trim(),
                                                     title: $(this).attr('href')
                                                   }))
                    })

                    let movieInfo = {
                        id: link.split("/").pop(),
                        title: title,
                        sensored: sensored === "有碼" ? 1 : 0,
                        coverImg: cover_img,
                        series: series,
                        magnets: magnets,
                        genre: genre,
                        artists: artists,
                        extrainfo: extraInfo,
                        ReleaseDate: extraInfo["ReleaseDate"]
                    }
                    saveObject("movie:" + movieInfo.id, movieInfo)
                    // add index
                    let movieScore = movieInfo.extrainfo["ReleaseDate"]
                    if (!movieScore) {
                        movieScore = 0
                    } else {
                        let datepart = movieScore.split("-")
                        let rdate = new Date(parseInt(datepart[0]), parseInt(datepart[1]) - 1, parseInt(datepart[2]))
                        movieScore = rdate.getTime()
                    }
                    if (movieInfo.series && movieInfo.series != "") {
                        redis.sadd("series:" + movieInfo.series + ":movies", movieInfo.id)
                        redis.zadd("series:" + movieInfo.series + ":moviesbydate", movieScore, movieInfo.id)
                        redis.sadd("series", movieInfo.series)
                    }
                    movieInfo.genre.forEach(function(item) {
                        redis.sadd("genre:" + item + ":movies", movieInfo.id)
                        redis.zadd("genre:" + item + ":moviesbydate", movieScore, movieInfo.id)
                        redis.sadd("genres", item)
                    })
                    movieInfo.artists.forEach(function(item) {
                        redis.sadd("artist:" + item + ":movies", movieInfo.id)
                    })
                    redis.sadd("movies", movieInfo.id)
                    return callback(null);
                });
        });

	function parse(script) {
		//console.log(script);
		let gid_r = /gid\s+=\s+(\d+)/g.exec(script);
		let gid = gid_r[1];
		let uc_r = /uc\s+=\s(\d+)/g.exec(script);
		let uc = uc_r[1];
		let img_r = /img\s+=\s+\'(\http:.+\.jpg)/g.exec(script);
		let img = img_r[1];
		return {
			gid: gid,
			img: img,
			uc: uc,
			lang: 'zh'
		};
	}
}



function pageExist(callback) {
	if (hasLimit && count < 2) return callback();
	let url = startUrl + (pageIndex === 1 ? '' : '/page/' + pageIndex);
	// console.log(url);
	console.log('获取第%d页中的影片链接 %s...'.green, pageIndex, url);
	let retryCount = 1;
	async.retry(3,
		function(callback, result) {
			request
				.get(url)
				.accept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
				.set('Accept-Encoding', 'gzip, deflate')
				.set('Connection', 'keep-alive')
				.timeout(timeout)
				.redirects(2)
				.end(function(err, res) {
					// console.log(res.status)
					if (err) {
						if (err.status === 404) {
							console.error('已抓取完所有页面,StatusCode:', err.status);
							return callback(err);
						} else {
							retryCount++;
							console.error('第%d页页面获取失败：%s'.red, pageIndex, err.message);
							console.error('...进行第%d次尝试...'.red, retryCount);
							return callback(err);
						}
					}
					currentPageHtml = res.text;
					callback(null, res);
				});
		},
		function(err, res) {
			retryCount = 3;
            if(err && err.status === 404){
                return callback(null,false);
            }
			if (err) {
				return callback(err);
			}
			callback(null, res.ok);
		});
}

exports.run = function() {
    index_page_queue.drain = function() {
      var delay = 60
      console.log("All indeces done, start all over again after %d minutes".blue, delay)
      setTimeout(function() {
        index_page_queue.push(baseUrl + "/actresses/")
        index_page_queue.push(baseUrl + "/uncensored/actresses/")
      }, delay * 60 * 1000)
    }

    index_page_queue.push(baseUrl + "/actresses/")
    index_page_queue.push(baseUrl + "/uncensored/actresses/")
}
