#!/usr/bin/env node

'use strict';
var vo = require('vo');
var Nightmare = require('nightmare');
var cheerio = require('cheerio');
var request = require('superagent');
var async = require('async');
var colors = require('colors');
var program = require('commander');
var ProgressBar = require('progress');
var userHome = require('user-home');
var path = require('path');
var fs = require('fs');
var Redis = require('ioredis')
var kickass = require('kickass-torrent')
var pretty = require('prettysize');

require('superagent-proxy')(request);

var drainWait = 60 * 60
var scraperCount = 5
var magnetGetterCount = 10
var retryCount = 3
var timeout = 15
var redisServer = process.env.redis_ip || 'redis'
var redisPort = process.env.redis_port || 6379
var proxy = process.env.http_proxy || 'http://192.168.11.5:3128';

console.log("Connecting to redis %s:%d".green, redisServer, redisPort)
var redis = new Redis(redisPort, redisServer)

function saveObject(basekey, obj) {
    var pipeline = redis.pipeline()
    for (var prop in obj) {
        let key = basekey + ":" + prop
        let value = obj[prop]
        if (Array.isArray(value)) {
            for (var i = 0; i < value.length; i++) {
                pipeline.sadd(key, value[i])
            }
        } else if (typeof(value) === "object") {
            for (var subkey in value) {
                pipeline.set(key + ":" + subkey, value[subkey])
            }
        } else {
            pipeline.set(key, value)
        }
    }
    pipeline.exec(function(err, result) {
        if (err) {
            console.log("Error in saving obj %s: %s".red, basekey, err);
            throw err
        }
        result.forEach(function(item) {
            if (item[0]) {
                console.log("Error in saving obj %s: %s".red, basekey, item[0]);
                throw item[0]
            }
        })
    })
}

var index_page_queue = async.queue(function(task, callback) {
  async.retry(retryCount, function(callback, result) {
    console.log("index page task %s".gray, task)
    console.log("via proxy %s".gray, proxy)
    request.get(task)
          .accept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
          .set('Accept-Encoding', 'gzip, deflate')
          .set('Connection', 'keep-alive')
          // .timeout(timeout)
          .redirects(4)
          .proxy(proxy)
          .end(function(err, res) {
            if (err) return callback(err)
            let $ = cheerio.load(res.text)
            $('ul#allmodels > li > a[href^="http://www.xart.com/models/"]').each(function() {
              var url = $(this).attr('href')
              if (!url) {
                console.log("Malformed url: %s".red, $(this))
                return
              }
              let coverImg = $(this).find('div.show-for-touch > div.cover > img').attr('src')
              let name = $(this).find('div.show-for-touch h1').text()
              let infos = []
              $(this).find('div.show-for-touch h2').each(function() {
                infos.push($(this).text())
              })
              console.log("Found artress %s".blue, name.red)
              console.log("\turl: %s".green, url.red)
              console.log("\tcover: %s".green, coverImg.red)
              console.log("\tcover: %s".green, infos)
              redis.exists("artist:" + name + ":id").then(function(result) {
                if (result == 0) {
                  let artist = {id: name, avatarUrl: coverImg, name: name, url: url}
                  saveObject("artist:" + name, artist)
                  redis.sadd("artists", name)
                }
              })
              // redis.sadd('artress', name)
              // redis.hmset('artress:' + name, 'name', name, 'cover', coverImg, 'infos', JSON.stringify(infos))
              artress_job_queue.push(url)
            })
            return callback(null, res)
          })
  },
  function(err, res) {
    if (err) {
      return callback(err)
    }
    return callback()
  })
})

var artress_job_queue = async.queue(function(task, callback) {
  async.retry(retryCount, function(callback, result) {
   request.get(task)
          .accept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
          .set('Accept-Encoding', 'gzip, deflate')
          .set('Connection', 'keep-alive')
          // .timeout(timeout)
          .redirects(4)
          .proxy(proxy)
          .end(function(err, res) {
            if (err) return callback(err)
            let $ = cheerio.load(res.text)
            $('ul#allupdates > li > a[href^="http://www.xart.com/videos/"]').each(function() {
              let url = $(this).attr('href')
              let name = $(this).find('div.show-for-touch h1').eq(0).text()
              let rating = $(this).find('div.show-for-touch h1').eq(1).text()
              let coverImg = $(this).find('div.show-for-touch div.cover > img').attr('src')
              console.log("Found movie %s (%s)".blue, name.yellow, rating.red)
              console.log("\turl: %s".green, url.red)
              console.log("\tcover image: %s".green, coverImg.red)
              movie_job_queue.push(url)
            })
            return callback(null, res)
          })
  },
  function(err, res) {
    if (err) {
      return callback(err)
    }
    return callback()
  })
})

var movie_job_queue = async.queue(function(task, callback) {
  async.retry(retryCount, function(callback, result) {
   request.get(task)
          .accept('text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
          .set('Accept-Encoding', 'gzip, deflate')
          .set('Connection', 'keep-alive')
          // .timeout(timeout)
          .redirects(4)
          .proxy(proxy)
          .end(function(err, res) {
            if (err) return callback(err)
            let $ = cheerio.load(res.text)
            let featuring = []
            let name = $('div.info h1').text()
            let imgUrl = $('div.video-tour img').attr('src')
            let id = "xart-" + name
            $('span:contains("featuring")').parent().find('a').each(function() {
              featuring.push($(this).text())
              redis.sadd('artist:'+$(this).text()+':movies', id)
            })
            let date = $($('h2').get(2)).text()

            console.log("Found movie detail:")
            console.log("\tfeaturing: %s".green, featuring)
            console.log("\tname: %s".green, name.red)
            console.log("\turl: %s".green, imgUrl.red)
            console.log("\tdate: %s".green, date.red)
            let movieInfo = {
              id: id,
              title: name,
              sensored: 0,
              coverImg: imgUrl,
              series: 'x-art',
              artists: featuring,
              ReleaseDate: date
            }
            saveObject("movie:" + id, movieInfo)
            redis.sadd('series', 'x-art')
            redis.sadd("series:" + movieInfo.series + ":movies", movieInfo.id)
            redis.zadd("series:" + movieInfo.series + ":moviesbydate", 0, movieInfo.id)
            movieInfo.artists.forEach(function(item) {
              redis.sadd("artist:" + item + ":movies", movieInfo.id)
            })
            redis.sadd("movies", movieInfo.id)

            console.log("fetching movie magnet %s".red, name)
            kickass({q: 'x-art ' + name}, function(err, response) {
              if (err) {
                console.log("Error in getting movie '%s'".red, name)
                return
              }
              console.log("Found kickass result %s (%d)".blue, response.title, response.total_results)
              console.log(response.list.length)
              for (var i=0; i<response.list.length; i++) {
                console.log("Found magnet: magnet:?xt=urn:btih:%s".blue, response.list[i].hash)
                let magnetUrl = 'magnet:?xt=urn:btih:' + response.list[i].hash
                // TODO use set to remove duplicate
                //{url: $(this).attr('href'), size: $(this).text().trim(), date: $(this).parent().next().children().first().text().trim()}
                let size = response.list[i].size
                let date = response.list[i].pubDate
                let title = response.list[i].title
                let magnetInfo = {url: magnetUrl, size: pretty(size), sizeInBytes: size, date: date, title: title}
                redis.sadd('movie:'+id+":magnets", JSON.stringify(magnetInfo))
              }
            })
            return callback(null, res)
          })
  },
  function(err, res) {
    if (err) {
      return callback(err)
    }
    return callback()
  })
})

exports.run = function() {
  index_page_queue.push("http://www.xart.com/models/")

  index_page_queue.drain = function() {
    var delay = 60
    console.log("All indeces done, start all over again after %d minutes".blue, delay)
    setTimeout(function() {
      index_page_queue.push("http://www.xart.com/models/")
    }, delay * 60 * 1000)
    // console.log("All done! exit!".blue)
    // process.exit()
  }
}
