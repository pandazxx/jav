package controllers

import (
	"fmt"
	"github.com/revel/revel"
)

func init() {
	revel.INFO.Println("Inited")
	fmt.Println("inited")
	revel.InterceptMethod((*LoggingController).Begin, revel.BEFORE)
	revel.InterceptMethod((*LoggingController).End, revel.FINALLY)
}
