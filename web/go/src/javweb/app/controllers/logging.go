package controllers

import (
	"fmt"
	"github.com/revel/revel"
	"runtime/debug"
)

type LoggingController struct {
	*revel.Controller
}

func (l *LoggingController) Begin() revel.Result {
	logInfo := map[string]string{
		"method": l.Request.Method,
		"uri":    l.Request.RequestURI,
	}
	revel.INFO.Printf("Incoming http request: %v", logInfo)
	return nil
}

func (l *LoggingController) End() revel.Result {
	debug.PrintStack()
	logInfo := map[string]string{
		"status":  fmt.Sprintf("%v", l.Response.Status),
		"headers": fmt.Sprintf("%v", l.Response.Out.Header()),
		// "bodylen": fmt.Sprintf("%v", len(l.Response.Out)),
	}
	revel.INFO.Printf("Response http response: %v", logInfo)

	return nil
}
