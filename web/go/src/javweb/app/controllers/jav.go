package controllers

import (
	"errors"
	"fmt"
	"io/ioutil"
	"javweb/app/models"
	"javweb/app/util"
	"net/http"
	"runtime/debug"
	"sort"

	"github.com/revel/revel"
)

type JavApp struct {
	*revel.Controller
	LoggingController
}

type JavResponse http.Response

func (r *JavResponse) Apply(req *revel.Request, resp *revel.Response) {
	revel.INFO.Println("Applying")
	contentType := "text/plain"

	debug.PrintStack()

	for k, v := range r.Header {
		if k == "Content-Type" {
			contentType = v[0]
		}

		// resp.Out.Header().Set(k, v...)
		for _, h := range v {
			resp.Out.Header().Add(k, h)
		}
	}
	resp.Out.Header().Set("X-zxx-header2", "2")
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		resp.WriteHeader(500, contentType)
		resp.Out.Write([]byte(fmt.Sprintf("Error in reading remote response: %v", err)))
		return
	}

	resp.WriteHeader(r.StatusCode, contentType)
	resp.Out.Write(body)
}

func (j *JavApp) Index() revel.Result {
	return j.Render()
}

func (j *JavApp) Artresses() revel.Result {
	redis, err := util.RedisPool().Get()
	if err != nil {
		// error handle
		return j.RenderError(err)
	}
	defer util.RedisPool().Put(redis)
	r := redis.Cmd("smembers", "artists")
	if r.Err != nil {
		return j.RenderError(r.Err)
	}
	ids, _ := r.List()
	artresses := models.ArtressesByIds(ids)
	revel.INFO.Printf("Found %d artresses", len(artresses))
	sort.Sort(util.NewUniSorter(artresses, func(i, j *models.Artress) bool {
		return len(j.MovieIds) < len(i.MovieIds)
	}))
	return j.Render(artresses)
}

func (j *JavApp) Artress(id string) revel.Result {
	artress := models.ArtressById(id)
	if artress == nil {
		return j.NotFound("Artress %v not found", id)
	}
	movies := models.MoviesByIds(artress.MovieIds)
	sort.Sort(util.NewUniSorter(movies, func(i, j *models.Movie) bool {
		return i.ExtraInfo().ReleaseDate > j.ExtraInfo().ReleaseDate
	}))
	return j.Render(artress, movies)
}

func (j *JavApp) Movie(id string) revel.Result {
	movie := models.MovieById(id)
	if movie == nil {
		return j.NotFound("Movie %v not found", id)
	}
	artresses := models.ArtressesByIds(movie.ArtressIds)

	revel.INFO.Printf("Found Moive %+v", movie)

	return j.Render(movie, artresses)
}

func (j *JavApp) Series(series string) revel.Result {
	movieids := models.MovieIdsBySeries(series)
	if len(movieids) == 0 {
		return j.NotFound("Series %v not found", series)
	}
	var movies []*models.Movie
	for _, id := range movieids {
		movies = append(movies, models.MovieById(id))
	}
	return j.Render(series, movies)
}

func (j *JavApp) Serieses() revel.Result {
	serieses := models.SeriesesSortedByMovieCount()
	return j.Render(serieses)
}

func (j *JavApp) Genres() revel.Result {
	genres := models.GenresOrderByMovieCount()
	return j.Render(genres)
}

func (j *JavApp) Genre(genre string) revel.Result {
	movieIds := models.MovieIdsFromGenre(genre)
	if len(movieIds) == 0 {
		return j.NotFound("Genre %v not found", genre)
	}
	var movies []*models.Movie
	for _, id := range movieIds {
		movies = append(movies, models.MovieById(id))
	}
	return j.Render(genre, movies)
}

func (j *JavApp) GetImage(url string) revel.Result {
	revel.TRACE.Printf("Proxying image request '%v'", url)
	if url == "" {
		return j.RenderError(errors.New("Wrong argument, url is missing"))
	}

	res, err := http.Get(url)
	if err != nil {
		return j.RenderError(err)
	}
	j.Response.Status = res.StatusCode
	j.Response.Out.Header().Set("X-zxx-header1", "1")

	return (*JavResponse)(res)
}
