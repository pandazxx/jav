package util

import (
	"fmt"
	"reflect"
)

type UniversalSorter struct {
	objToBeSorted interface{}
	sortFunc      interface{}
}

func NewUniSorter(tobesorted interface{}, sortFunc interface{}) UniversalSorter {
	sortkind := reflect.TypeOf(tobesorted).Kind()
	funckind := reflect.TypeOf(sortFunc).Kind()
	if sortkind != reflect.Slice {
		panic(fmt.Sprintf("Wrong type of sorting object provided %v, expected %v", sortkind, reflect.Slice))
	}
	if funckind != reflect.Func {
		panic(fmt.Sprintf("Wrong type of sorting function, provided %v, expected %v", funckind, reflect.Func))
	}
	return UniversalSorter{
		objToBeSorted: tobesorted,
		sortFunc:      sortFunc,
	}
}

func (u UniversalSorter) Len() int {
	value := reflect.ValueOf(u.objToBeSorted)
	return value.Len()
}

func (u UniversalSorter) Swap(i, j int) {
	sv := reflect.ValueOf(u.objToBeSorted)
	iv := sv.Index(i)
	jv := sv.Index(j)
	tmp := iv.Interface()
	iv.Set(jv)
	jv.Set(reflect.ValueOf(tmp))
}

func (u UniversalSorter) Less(i, j int) bool {
	fv := reflect.ValueOf(u.sortFunc)
	sv := reflect.ValueOf(u.objToBeSorted)
	iv := sv.Index(i)
	jv := sv.Index(j)
	args := [...]reflect.Value{iv, jv}
	result := fv.Call(args[:])
	return result[0].Bool()
}
