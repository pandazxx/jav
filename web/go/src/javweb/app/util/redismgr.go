package util

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/revel/revel"
	"reflect"
	"strings"

	"github.com/mediocregopher/radix.v2/pool"
)

var redisPool *pool.Pool

func InitRedis(ip string, port int) error {
	if redisPool != nil {
		return errors.New("Double init!")
	}
	addr := fmt.Sprintf("%v:%d", ip, port)
	p, err := pool.New("tcp", addr, 10)
	redisPool = p
	if err != nil {
		return err
	}
	return nil
}

func RedisPool() *pool.Pool {
	return redisPool
}

type redisObjType int

const (
	str redisObjType = iota
	list
	set
	sortedSet
)

type redisCmdBuf struct {
	RedisName      string
	RedisType      redisObjType
	FieldName      string
	FieldIndex     int
	FieldType      reflect.Kind
	ElemEncodeType string
}

func (r redisCmdBuf) MakeCommand(basekey string) []interface{} {
	var ret []interface{}
	switch r.RedisType {
	case str:
		ret = append(ret, "get", basekey+":"+r.RedisName)
	case list:
		ret = append(ret, "lrange", basekey+":"+r.RedisName, 0, -1)
	case set:
		ret = append(ret, "smembers", basekey+":"+r.RedisName)
	case sortedSet:
		ret = append(ret, "zrange", basekey+":"+r.RedisName, 0, -1)
	}
	return ret
}

type redisTypeBuf struct {
	ObjType reflect.Type
	CmdBufs []redisCmdBuf
}

var redisTypeBufs = make(map[reflect.Type]redisTypeBuf)

func getRedisCmdBuf(objtype reflect.Type) redisTypeBuf {
	cmdBuf, ok := redisTypeBufs[objtype]
	if !ok {
		cmdBuf.ObjType = objtype
		for i := 0; i < objtype.NumField(); i++ {
			field := objtype.Field(i)
			ignoreTag := field.Tag.Get("ignore")
			if ignoreTag == "true" {
				continue
			}
			typeTag := field.Tag.Get("type")
			nameTag := field.Tag.Get("name")
			elemTag := field.Tag.Get("elem")
			if typeTag == "" {
				switch field.Type.Kind() {
				case reflect.Array, reflect.Slice:
					typeTag = "list"
				default:
					typeTag = "string"
				}
			}
			if nameTag == "" {
				nameTag = strings.ToLower(field.Name)
			}
			var buf redisCmdBuf
			buf.FieldIndex = i
			buf.FieldName = field.Name
			buf.FieldType = field.Type.Kind()
			buf.RedisName = nameTag
			buf.ElemEncodeType = elemTag
			switch typeTag {
			case "list":
				buf.RedisType = list
			case "string":
				buf.RedisType = str
			case "sortedset":
				buf.RedisType = sortedSet
			case "set":
				buf.RedisType = set
			}
			cmdBuf.CmdBufs = append(cmdBuf.CmdBufs, buf)
		}
		redisTypeBufs[objtype] = cmdBuf
	}
	return cmdBuf
}

func RedisToObject(basekey string, obj interface{}) error {
	rclient, err := RedisPool().Get()
	if err != nil {
		return err
	}
	defer RedisPool().Put(rclient)
	defer rclient.PipeClear()

	// objtype := reflect.TypeOf(obj)
	redisbuf := getRedisCmdBuf(reflect.TypeOf(obj).Elem())
	// for _, cmd := range redisbuf.CmdBufs {
	// 	cmds := cmd.MakeCommand(basekey)
	// 	fmt.Printf("Executing redis command %+v\n", cmds)
	// 	rclient.PipeAppend(cmds[0].(string), cmds[1:]...)
	// }

	for _, cmd := range redisbuf.CmdBufs {
		// fmt.Printf("cmd: %+v\n", cmd)
		commandArray := cmd.MakeCommand(basekey)
		switch cmd.RedisType {
		case str:
			// v, err := rclient.PipeResp().Str()
			v, err := rclient.Cmd(commandArray[0].(string), commandArray[1:]).Str()
			if err != nil {
				return fmt.Errorf("Error in loading redis string with cmd: %+v, %+v, error: %v", cmd, cmd.MakeCommand(basekey), err)
			}
			// fmt.Printf("object: %v, v: %v field\n", obj, v, reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex))
			// fmt.Printf("Loading redis string with cmd: %+v, %+v\n", cmd, cmd.MakeCommand(basekey))
			switch cmd.FieldType {
			case reflect.Bool:
				boolResult := v == "1"
				reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex).SetBool(boolResult)
			default:
				reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex).Set(reflect.ValueOf(v))
			}
		case list, set, sortedSet:
			// v, err := rclient.PipeResp().List()
			v, err := rclient.Cmd(commandArray[0].(string), commandArray[1:]...).List()
			if err != nil {
				return fmt.Errorf("Error in loading redis list with cmd: %+v, %+v, error: %v", cmd, cmd.MakeCommand(basekey), err)
			}
			switch cmd.ElemEncodeType {
			case "json":
				elemType := redisbuf.ObjType.Field(cmd.FieldIndex).Type.Elem()
				slice := reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex)
				for _, i := range v {
					elemValue := reflect.New(elemType)
					elem := elemValue.Interface()
					// fmt.Printf("New Elem %+v\n", elem)
					revel.INFO.Printf("zxx debug %v", i)
					json.Unmarshal([]byte(i), &elem)
					// fmt.Printf("Result Elem %+v\n", elem)
					revel.INFO.Printf("zxx debug 2 %+v", reflect.ValueOf(elem).Elem())
					slice = reflect.Append(slice, reflect.ValueOf(elem).Elem())
				}
				reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex).Set(slice)
			default:
				// fmt.Printf("object: %v, v: %v field\n", obj, v, reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex))
				// fmt.Printf("Loading redis list with cmd: %+v, %+v\n", cmd, cmd.MakeCommand(basekey))
				reflect.ValueOf(obj).Elem().Field(cmd.FieldIndex).Set(reflect.ValueOf(v))
			}
		}
	}
	return nil
}

func RedisSetOfKey(key string, properties ...string) map[string][]string {
	result := make(map[string][]string)
	rc, err := RedisPool().Get()
	if err != nil {
		return result
	}
	defer RedisPool().Put(rc)
	defer rc.PipeClear()

	for _, p := range properties {
		pk := key + p
		rc.PipeAppend("smembers", pk)
	}

	for _, p := range properties {
		value, err := rc.PipeResp().List()
		if err != nil {
			return result
		}

		result[p] = value
	}

	return result
}

func RedisStringsOfKey(key string, properties ...string) map[string]string {
	result := make(map[string]string)
	rc, err := RedisPool().Get()
	if err != nil {
		return result
	}
	defer RedisPool().Put(rc)
	defer rc.PipeClear()

	for _, p := range properties {
		pk := key + p
		rc.PipeAppend("get", pk)
	}

	for _, p := range properties {
		value, err := rc.PipeResp().Str()
		if err != nil {
			return result
		}
		result[p] = value
	}

	return result
}
