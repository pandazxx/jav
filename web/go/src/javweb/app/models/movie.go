package models

import (
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/revel/revel"
	"javweb/app/util"
	"sort"
)

/*
{
url: $(this).attr('href'),
size: $(this).text().trim(),
date: $(this).parent().next().children().first().text().trim()
}
*/

type MagnetInfo struct {
	Url         string
	Size        string
	Date        string
	Title       string
	SizeInBytes uint64
}

func (m *MagnetInfo) GetSize() string {
	if m.Size == "" {
		m.Size = humanize.Bytes(uint64(m.SizeInBytes))
	}

	return m.Size
}

func (m *MagnetInfo) GetSizeInBytes() uint64 {
	if m.SizeInBytes == 0 {
		SizeInBytes, err := humanize.ParseBytes(m.Size)
		m.SizeInBytes = SizeInBytes
		if err != nil {
			m.SizeInBytes = 0
		}
	}

	return m.SizeInBytes
}

/*
var EXTRA_INFO_KEY_MAPPING = {
    '發行日期': 'ReleaseDate',
    '長度': 'Length',
}

*/
type MovieExtraInfo struct {
	ReleaseDate string `name:"ReleaseDate"`
	Length      string `name:"Length"`
}

/*
let movieInfo = {
                        id: link.split("/").pop(),
                        title: title,
                        sensored: sensored === "有碼" ? 1 : 0,
                        coverImg: cover_img,
                        series: series,
                        magnets: magnets,
                        genre: genre,
                        artists: artists,
                        extrainfo: extraInfo,
                    }
*/

type Movie struct {
	Id         string          `name:"id"`
	Title      string          `name:"title"`
	ArtressIds []string        `name:"artists" type:"set"`
	Censored   bool            `name:"sensored"`
	Series     string          `name:"series"`
	Genres     []string        `name:"genre" type:"set"`
	MagnetUrls []*MagnetInfo   `name:"magnets" type:"set" elem:"json"`
	CoverUrl   string          `name:"coverImg"`
	extraInfo  *MovieExtraInfo `ignore:"true"`
}

func (m *Movie) ExtraInfo() *MovieExtraInfo {
	if m.extraInfo == nil {
		var extraInfo MovieExtraInfo
		util.RedisToObject("movie:"+m.Id+":extrainfo", &extraInfo)
		m.extraInfo = &extraInfo
	}
	return m.extraInfo
}

func (m *Movie) BestMagnets() []*MagnetInfo {
	magnets := m.MagnetUrls[:]
	sort.Sort(util.NewUniSorter(magnets, func(i, j *MagnetInfo) bool {
		return i.GetSizeInBytes() > j.GetSizeInBytes()
	}))
	end := len(magnets)
	if end > 3 {
		end = 3
	}
	if len(magnets) > 0 {
		revel.INFO.Printf("Got best magnets: %+v", magnets[0])
	}
	return magnets[0:end]
}

func (m *Movie) Artresses() []*Artress {
	return ArtressesByIds(m.ArtressIds)
}

func MovieById(id string) *Movie {
	var ret Movie
	err := util.RedisToObject("movie:"+id, &ret)
	if err != nil {
		fmt.Printf("Error in getting movie %v: %v\n", id, err)
		return nil
	}
	return &ret
}

func MoviesByIds(ids []string) []*Movie {
	var movies []*Movie
	for _, id := range ids {
		movie := MovieById(id)
		if movie != nil {
			movies = append(movies, movie)
		}
	}
	return movies
}
