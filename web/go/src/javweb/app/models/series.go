package models

import (
	"fmt"
	"javweb/app/util"
)

func SeriesesSortedByMovieCount() []string {
	rclient, err := util.RedisPool().Get()
	if err != nil {
		return []string{}
	}
	defer util.RedisPool().Put(rclient)
	exists, err := rclient.Cmd("exists", "seriesbymoviecnt").Int()
	if err != nil {
		return []string{}
	}
	if exists == 0 {
		// populate the cache
		serieses, err := rclient.Cmd("smembers", "series").List()
		if err != nil {
			return []string{}
		}
		for _, series := range serieses {
			rclient.PipeAppend("scard", fmt.Sprintf("series:%s:movies", series))
		}
		var movieCounts []int
		for _ = range serieses {
			movieCount, err := rclient.PipeResp().Int()
			if err != nil {
				movieCount = 0
			}
			movieCounts = append(movieCounts, movieCount)
		}

		for i, series := range serieses {
			rclient.PipeAppend("zadd", "seriesbymoviecnt", movieCounts[i], series)
		}
		for _ = range serieses {
			rclient.PipeResp()
		}
	}
	result, err := rclient.Cmd("zrevrange", "seriesbymoviecnt", 0, -1).List()
	if err != nil {
		return []string{}
	}
	return result
}

func MovieIdsBySeries(series string) []string {
	rclient, err := util.RedisPool().Get()
	if err != nil {
		return []string{}
	}
	defer util.RedisPool().Put(rclient)
	ret, err := rclient.Cmd("zrevrange", "series:"+series+":moviesbydate", 0, -1).List()
	if err != nil {
		return []string{}
	}
	return ret
}
