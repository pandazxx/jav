package models

import (
	"fmt"
	"javweb/app/util"
)

// GenresOrderByMovieCount Get all genre order by movie count
func GenresOrderByMovieCount() []string {
	rclient, err := util.RedisPool().Get()
	if err != nil {
		return []string{}
	}
	defer util.RedisPool().Put(rclient)
	defer rclient.PipeClear()

	exists, _ := rclient.Cmd("exists", "genresbymoviecount").Int()
	if exists == 0 {
		genres, err := rclient.Cmd("smembers", "genres").List()
		if err != nil {
			return []string{}
		}
		for i := range genres {
			rclient.PipeAppend("scard", fmt.Sprintf("genre:%s:movies", genres[i]))
		}
		for i := range genres {
			movieCount, _ := rclient.PipeResp().Int()
			rclient.PipeAppend("zadd", "genresbymoviecount", movieCount, genres[i])
		}
		for _ = range genres {
			rclient.PipeResp()
		}
	}
	orderedGenres, _ := rclient.Cmd("zrevrange", "genresbymoviecount", 0, -1).List()
	return orderedGenres
}

// MovieIdsFromGenre get all movie id in genre order by release date
func MovieIdsFromGenre(genre string) []string {
	rclient, err := util.RedisPool().Get()
	if err != nil {
		return []string{}
	}
	defer util.RedisPool().Put(rclient)
	defer rclient.PipeClear()
	movieIds, _ := rclient.Cmd("zrevrange", fmt.Sprintf("genre:%s:moviesbydate", genre), 0, -1).List()
	fmt.Printf("Found %d movies", len(movieIds))
	return movieIds
}
