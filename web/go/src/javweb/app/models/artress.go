package models

import (
	"github.com/revel/revel"
	// "github.com/mediocregopher/radix.v2/redis"
	"javweb/app/util"
)

/*

   let artist = {id: id, avatarUrl: avatarUrl, name: name, url: url}
*/

type Artress struct {
	Id        string   `name:"id"`
	Name      string   `name:"name"`
	AvatarUrl string   `name:"avatarUrl"`
	MovieIds  []string `name:"movies" type:"set"`
}

func ArtressesByIds(ids []string) []*Artress {
	var artresses []*Artress
	for _, id := range ids {
		var artress Artress
		err := util.RedisToObject("artist:"+id, &artress)
		if err != nil {
			revel.WARN.Printf("Error in loading %v artress: %v", id, err)
			continue
		}
		artresses = append(artresses, &artress)
	}
	return artresses
}

func ArtressById(id string) *Artress {
	var artress Artress
	err := util.RedisToObject("artist:"+id, &artress)
	if err != nil {
		return nil
	}
	return &artress
}
