#!/usr/bin/env bash

docker build -t jav/app app && \
 docker build -t jav/nginx nginx && \
 docker build -t jav/redis redis && \
 docker build -t jav/sock sockstore