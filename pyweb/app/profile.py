#!/usr/bin/env python
# -*- coding: utf-8 -*-

from werkzeug.contrib.profiler import ProfilerMiddleware
import server

server.app.config['PROFILE'] = True
server.app.debug = True
server.app.wsgi_app = ProfilerMiddleware(server.app.wsgi_app, profile_dir="/tmp")
server.main()
