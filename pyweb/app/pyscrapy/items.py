# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Movie(scrapy.Item):
    id = scrapy.Field()
    series = scrapy.Field()
    title = scrapy.Field()
    # artressIds = model.ContainerField(name="artists")
    censored = scrapy.Field()
    coverUrl = scrapy.Field()
    releaseDate = scrapy.Field()
    artresses = scrapy.Field()


class Artress(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    avatarUrl = scrapy.Field()


# _MAGNET_KEYS = ('url', 'size', 'sizeInBytes', 'date', 'title')
class Magnet(scrapy.Item):
    url = scrapy.Field()
    size = scrapy.Field()
    date = scrapy.Field()
    title = scrapy.Field()
