#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scrapy
from scrapy.selector import Selector
import re
import config
import json
from models import model, db
from datetime import datetime
import hashlib


db.setup(config.db_conn_str)
session = db.new_session()


def artress_id_by_name(name):
    return hashlib.md5(("xart-" + name).encode('utf-8')).hexdigest()


class JavSpider(scrapy.Spider):
    name = "xart"
    # allowed_domains = ["dmoz.org"]
    # http://www.xart.com/index.php?show=model&pref=items&page=1&order=popularity&catname=
    # start_urls = [
    #     "http://www.javbus.in/actresses/",
    #     "https://www.javbus.in/uncensored/actresses"
    # ]
    _default_header = {'Referer': 'http://www.xart.com/videos/hrated/all/'}

    def _movie_list_url(self, page=1):
        return 'http://www.xart.com/index.php?show=videos&pref=items&page=%d&catname=All&order=hrated' % page

    def _magnet_search_request(self, keywords):
        import urllib
        query= {'q': keywords, 'order': 'desc'}
        base_url = 'https://kat.cr/json.php'
        url = base_url + "?" + urllib.urlencode(query)
        return scrapy.Request(url, callback=self.parse_movie_magnets)

    def start_requests(self):
        yield scrapy.Request(self._movie_list_url(1),
                             headers=self._default_header,
                             callback=self.parse)

    def parse(self, response):
        json_resp = json.loads(response.body)
        html = json_resp['html']
        selector = Selector(text=html)
        for url in selector.xpath('/html/body/li/a[1]/@href').extract():
            self.logger.info("Crawling movie page %s", url)
            yield scrapy.Request(url,
                                 headers=self._default_header,
                                 callback=self.parse_movie_page)
        if 'next' in json_resp:
            next_page = json_resp['next']
            yield scrapy.Request(self._movie_list_url(next_page),
                                 headers=self._default_header,
                                 callback=self.parse)

    def parse_movie_page(self, response):
        movie_id = response.url.strip('/').split('/')[-1]
        movie_title = response.xpath('/html/body/div[4]/div/section/div[2]/div[1]/div/h1/text()')[0].extract()
        try:
            movie_cover = response.xpath('/html/body/div[4]/div/section/div[2]/div[2]/div/a/img/@src')[0].extract()
        except:
            t = response.xpath('/html/body/div[4]/div/section/div[2]/div[2]/div/a/img/@data-interchange')[0].extract()
            print t
            movie_cover = re.search(r'\[([^\]]*)\]', t).groups()[0].split(',')[0]
        movie_obj = session.query(model.Movie).get(movie_id)
        if movie_obj:
            return
        try:
            date_str = response.xpath('/html/body/div[4]/div/section/div[2]/div[3]/div/h2[1]/text()')[0].extract().strip()
            release_date = datetime.strptime(date_str, '%b %d, %Y')
        except:
            release_date = datetime(2001, 1, 1, 0, 0)
        series = model.get_or_new_series("x-art", session)
        movie_obj = model.Movie()
        movie_obj.id = movie_id
        movie_obj.censored = False
        movie_obj.updatedDate = datetime.now()
        movie_obj.releaseDate = release_date
        movie_obj.series = series
        movie_obj.title = movie_title
        movie_obj.coverUrl = movie_cover

        try:
            session.add(movie_obj)
            session.commit()
        except Exception as e:
            session.rollback()
            raise e

        self.logger.info("Parsed movie %s(%s) cover:[%s]", movie_title, movie_id, movie_cover)
        magnet_request = self._magnet_search_request('x-art ' + movie_title)
        magnet_request.meta["movie_id"] = movie_id
        yield magnet_request
        for elem in response.xpath('/html/body/div[4]/div/section/div[2]/div[3]/div/h2[2]/a'):
            href = elem.xpath('@href')[0].extract()
            name = elem.xpath('text()')[0].extract() + " (x-art)"
            id = artress_id_by_name(name)
            artress_obj = session.query(model.Artress).get(id)
            if artress_obj:
                movie_obj.artresses.add(artress_obj)
                try:
                    session.commit()
                except:
                    session.rollback()
                    raise
                continue
            self.logger.info("Crawling artress %s", href)
            yield scrapy.Request(href,
                                 callback=self.parse_artress_detail,
                                 meta={"movie_id": movie_id, "artress_name": name})

    def parse_artress_detail(self, response):
        movie_id = response.meta['movie_id']
        # artress_name = response.xpath('/html/body/div[4]/div/section/div[2]/div[1]/div[1]/h1/text()')[0].extract()
        artress_name = response.meta['artress_name']
        try:
            artress_avatarurl = response.xpath('/html/body/div[4]/div/section/div[2]/div[1]/div[2]/img/@src')[0].extract()
        except:
            # from scrapy.shell import inspect_response
            # inspect_response(response, self)
            t = response.xpath('/html/body/div[4]/div/section/div[2]/div[1]/div[2]/img/@data-interchange')[0].extract()
            artress_avatarurl = re.search(r'\[([^\]]*)\]', t).groups()[0].split(',')[0]
        self.logger.info("Found artress %s[%s]", artress_name, artress_avatarurl)
        artress_id = artress_id_by_name(artress_name)
        artress_obj = session.query(model.Artress).get(artress_id)
        movie_obj = session.query(model.Movie).get(movie_id)
        try:
            if not artress_obj:
                artress_obj = model.Artress()
                artress_obj.id = artress_id
                artress_obj.name = artress_name
                artress_obj.avatarUrl = artress_avatarurl
                session.add(artress_obj)
            movie_obj.artresses.add(artress_obj)
            artress_obj.movies.add(movie_obj)
            session.commit()
        except Exception as e:
            self.logger.error("Error in saving artress <%s>, %s", artress_name, e)
            session.rollback()

    def parse_movie_magnets(self, response):
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)
        movie_id = response.meta['movie_id']
        movie_obj = session.query(model.Movie).get(movie_id)
        j = json.loads(response.body)
        for item in j['list']:
            title = item['title']
            hash = item['hash']
            size = item['size']
            url = 'magnet:?xt=urn:btih:' + hash
            self.logger.info("Found magnet <%s> %s(%s)[%d]", response.meta["movie_id"], title, url, size)
            magnet_obj = model.get_or_new_magnet(hash, url, session, title=title)
            magnet_obj.size = size
            movie_obj.magnets.add(magnet_obj)
        try:
            session.commit()
        except Exception as e:
            self.logger.error("Error in commit magnet: %s", e)
            session.rollback()


