#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scrapy
import config
from models import model
from models import redismgr
from models import db
from models import cachectl
import hashlib
redismgr.connection_setup(host=config.redis_host, port=config.redis_port)


db.setup(config.db_conn_str)
session = db.new_session()

#
# @app.route("/getimage")
# def image_proxy():
#     url = flask.request.args.get('url', None)
#     if not url:
#         url = 'http://docs.python-requests.org/en/latest/_static/requests-sidebar.png'
#     cache_key = "imgcache:" + hashlib.sha1(url).hexdigest()
#     cache = cachectl.load_cache(cache_key)
#     if cache:
#         content, status, headers = cache
#         # print "Hit cache %s(%s)" %(url, cache_key)
#         return (content, status, headers)
#     r = requests.get(url, stream=True)
#
#     def stream():
#         cache_body = ""
#         for content in r.iter_content(100):
#             cache_body += content
#             yield content
#         cachectl.save_cache(cache_key,
#                             (cache_body, r.status_code, r.headers.items()))
#     return Response(stream(), status=r.status_code, headers=r.headers.items())


class ImageSpider(scrapy.Spider):
    name = "image"

    def start_requests(self):
        for artress in session.query(model.Artress):
            url = artress.avatarUrl
            cache_key = "imgcache:" + hashlib.sha1(url).hexdigest()
            cache = cachectl.load_cache(cache_key)
            if cache:
                self.logger.info("Passed exists cache %s", url)
                continue
            yield scrapy.Request(url, callback=self.parse)
        for movie in session.query(model.Movie):
            url = movie.coverUrl
            cache_key = "imgcache:" + hashlib.sha1(url).hexdigest()
            cache = cachectl.load_cache(cache_key)
            if cache:
                self.logger.info("Passed exists cache %s", url)
                continue
            yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        url = response.url
        cache_key = "imgcache:" + hashlib.sha1(url).hexdigest()
        body = response.body
        status = response.status
        headers = response.headers.items()
        cachectl.save_cache(cache_key, (body, status, headers))
        self.logger.info("Saved cache <%s> %s", cache_key, url)

