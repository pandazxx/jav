#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scrapy
from datetime import datetime
import config
from models import model, db
from urlparse import urlparse, parse_qs

db.setup(config.db_conn_str)
session = db.new_session()


def parse_magnet_url(url):
    try:
        if type(url) == unicode:
            url = url.encode('utf-8')
        qs = parse_qs(urlparse(url).query)
        hash = qs['xt'][0].replace('urn:btih:', '')
        title = qs['dn'][0].decode('utf-8')
        ret = model.get_or_new_magnet(hash, url, session, title=title)
        return ret
    except Exception as e:
        return None


class JavSpider(scrapy.Spider):
    name = "jav"
    # allowed_domains = ["dmoz.org"]
    # http://www.xart.com/index.php?show=model&pref=items&page=1&order=popularity&catname=
    start_urls = [
        "http://www.javbus.in/actresses/",
        "https://www.javbus.in/uncensored/actresses"
    ]

    def parse(self, response):
        return self.parse_artress_list_page(response)

    def parse_artress_list_page(self, response):
        artress_urls = response.css("a.avatar-box").xpath('@href').extract()
        for url in artress_urls:
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse_artress_page)
        next_url = response.xpath(u"//a[text()='下一頁']/@href").extract()
        if next_url and len(next_url) > 0:
            url = response.urljoin(next_url[0])
            yield scrapy.Request(url, callback=self.parse_artress_list_page)

    def parse_artress_page(self, response):
        censored = True
        if '/uncensored/' in response.url:
            censored = False
        for url in response.css("a.movie-box").xpath('@href').extract():
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse_movie_page, meta={'censored': censored})

    def parse_movie_page(self, response):
        from datetime import datetime
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)
        # script = response.xpath('body/script').extract()[3]
        # if not script or len(script) == 0:
        #     # handle error
        #     return
        import re
        script = None
        for s in response.xpath('body/script').extract():
            if re.match(r'.*gid\s+=\s+(\d+).*', s, re.S):
                script = s
                break
        if not script or len(script) == 0:
            # should handle error
            self.logger.error("Error in parsing magnet: %s, cannot find script", response.url)
            return

        artresses = set()
        for node in response.css('div.star-box.star-box-common.star-box-up.idol-box > li > a'):
            # artress_url = node.xpath('@href')[0].extract()
            import hashlib
            name = node.xpath('img/@title')[0].extract()
            artress_id = hashlib.md5(name.encode('utf-8')).hexdigest()
            avatar_url = node.xpath('img/@src')[0].extract()
            artress = session.query(model.Artress).get(artress_id)
            if not artress:
                artress = model.Artress()
                artress.id = artress_id
                artress.name = name
                artress.avatarUrl = avatar_url
                session.add(artress)
            artresses.add(artress)

        # class Movie(scrapy.Item):
        #     id = scrapy.Field()
        #     series = scrapy.Field()
        #     title = scrapy.Field()
        #     # artressIds = model.ContainerField(name="artists")
        #     censored = scrapy.Field()
        #     coverUrl = scrapy.Field()
        #     releaseDate = scrapy.Field()
        # movie = items.Movie()
        movie_id = response.url.split("/")[-1]
        movie = session.query(model.Movie).filter_by(id=movie_id).one_or_none()
        if not movie:
            movie = model.Movie()
            movie.id = movie_id
            movie.updatedDate = datetime.now()
        else:
            # bypass known movies
            return
        movie.title = response.xpath('//h3/text()')[0].extract()
        movie.coverUrl = response.css('div.col-md-9.screencap > a.bigImage > img')[0].xpath('@src').extract()
        series = response.xpath(u"//span[text()='系列:']/following-sibling::a/text()").extract()
        if series and len(series) > 0:
            series = series[0]
        else:
            series = None
        if series:
            movie.series = model.get_or_new_series(series, session)
        movie.censored = response.meta['censored']
        release_date_str = response.xpath(u"//span[text()='發行日期:']/parent::*/text()")[0].extract()
        self.logger.debug("Parsing date %s", release_date_str)
        try:
            date = datetime.strptime(release_date_str.strip(), "%b %d, %Y")
            # movie['releaseDate'] = date
        except:
            try:
                date = datetime.strptime(release_date_str.strip(), "%Y-%m-%d")
                # movie['releaseDate'] = date
            except:
                self.logger.error("Error in parsing date '%s', url: '%s'", release_date_str, response.url)
                date = datetime.strptime('1970-01-01', "%Y-%m-%d")
        movie.releaseDate = date
        for artress in artresses:
            movie.artresses.add(artress)
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)
        try:
            session.add(movie)
            session.commit()
        except Exception as e:
            self.logger.error("Error in saving movie: %s", e)
            session.rollback()
            raise e
        try:
            import re
            import random
            gid = re.search(r'gid\s+=\s+(\d+)', script).group(1)
            uc = re.search(r'uc\s+=\s+(\d+)', script).group(1)
            img = re.search(r"img\s+=\s+\'(https?:.+\.jpg)\'", script).group(1)
            lang = 'zh'
            floor = int(random.random() * 1e3 + 1)
            magnet_url = "/ajax/uncledatoolsbyajax.php?gid=%s&lang=%s&img=%s&uc=%s&floor=%d" % (gid, lang, img, uc, floor)
            magnet_url = response.urljoin(magnet_url)
            yield scrapy.Request(magnet_url, callback=self.parse_magnet_request, meta={'movie_id': movie.id})
        except Exception as e:
            self.logger.error("Error in parsing magnet: %s, script '%s'", e, script)
            # raise e

    def parse_magnet_request(self, response):
        import humanfriendly
        movie_id = response.meta['movie_id']
        movie = session.query(model.Movie).get(movie_id)
        if not movie:
            self.logger.error("Cannot found movie %s", movie_id)
            return
        for node in response.css("tr td:nth-child(2) a"):
            url = node.xpath('@href').extract()[0].strip()
            sizeStr = node.xpath('text()').extract()[0].strip()
            size = 0
            try:
                size = humanfriendly.parse_size(sizeStr)
            except:
                pass
            magnet = parse_magnet_url(url)
            if not magnet:
                continue
            magnet.size = size
            movie.magnets.add(magnet)
        try:
            session.commit()
        except Exception as e:
            self.logger.error("Error in commit magnet: %s", e)
            session.rollback()
