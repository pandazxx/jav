function star(type, id, star) {
    url = api_root + type + "/" + id
    $.ajax({
        type: "PUT",
        url: url,
        data: {star: star},
        success: function(data) {
            console.log(data)
            location.reload()
        }
    })
}