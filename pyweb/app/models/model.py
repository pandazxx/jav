#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, String, Boolean, DateTime, BigInteger, Text, LargeBinary, Integer
from sqlalchemy import ForeignKey
from sqlalchemy import Table
from sqlalchemy import select
from sqlalchemy.orm import column_property
from sqlalchemy import func
from sqlalchemy import desc
from sqlalchemy import and_


Base = declarative_base()


movie_artress = Table('movie_artress', Base.metadata,
    Column('movie_id', ForeignKey('movies.id'), primary_key=True),
    Column('artress_id', ForeignKey('artresses.id'), primary_key=True))

movie_genre = Table('movie_genre', Base.metadata,
    Column('movie_id', ForeignKey('movies.id'), primary_key=True),
    Column('genre_name', ForeignKey('genre.name'), primary_key=True))


class Movie(Base):
    __tablename__ = 'movies'
    id = Column(String(255), primary_key=True)
    title = Column(String(255))
    # artressIds = model.ContainerField(name="artists")
    censored = Column(Boolean)
    series_name = Column(String(255), ForeignKey('serieses.name'))
    series = relationship('Series', back_populates='movies')
    genres = relationship('Genre',
                          secondary=movie_genre,
                          back_populates='movies',
                          # lazy='dynamic',
                          collection_class=set)
    coverUrl = Column(String(255))
    # magnetUrls = model.ContainerField(name='magnets',
    #                                   encoder=dumpMagnetInfo,
    #                                   decoder=parseMagnetInfo,
    #                                   default=set())
    releaseDate = Column(DateTime)
    artresses = relationship('Artress',
                             secondary=movie_artress,
                             back_populates='movies',
                             # lazy='dynamic',
                             collection_class=set)
    score = Column(Integer)


    magnets = relationship('MagnetInfo', collection_class=set)
    _best_magnets = None
    updatedDate = Column(DateTime)

    @property
    def bestMagnets(self):
        if not self._best_magnets:
            cnt = 3 if len(self.magnets) > 3 else len(self.magnets)
            self._best_magnets = sorted(self.magnets, cmp=lambda x, y: int(x.size - y.size), reverse=True)[0:cnt]
        return self._best_magnets

    def __repr__(self):
        return u"Movie(id='{}', title='{}', censored={}, series='{}', genres='{}', releaseDate='{}'')".format(
            self.id, self.title, self.censored, self.series, self.genres, self.releaseDate)

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    def to_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'censored': self.censored,
            'series_name': self.series_name,
            # 'genres': [g.name for g in self.genres],
            'coverUrl': self.coverUrl,
            # 'releaseDate': self.releaseDate,
            # 'artresses': [a.id for a in self.artresses],
            # 'magnets': [m.hash for m in self.magnets],
            # 'updatedDate': self.updatedDate,
            'artresses_cnt': self.artresses_cnt,
        }


class MagnetInfo(Base):
    __tablename__ = 'magnets'
    hash = Column(String(1024), primary_key=True)
    url = Column(Text)
    size = Column(BigInteger)
    date = Column(DateTime)
    title = Column(Text)
    movie_id = Column(String(255), ForeignKey('movies.id'))
    movie = relationship('Movie', back_populates='magnets')
    __size_str = None

    @property
    def size_str(self):
        if self.__size_str is None:
            import humanfriendly
            self.__size_str = humanfriendly.format_size(self.size)
        return self.__size_str

    def __eq__(self, other):
        return self.hash == other.hash

    def __hash__(self):
        return hash(self.hash)


class Artress(Base):
    _STAR_SCORE = 2000
    _MONOPOLIZE_SCORE = 100
    _STAR_SERIES_SCORE = 100
    _MOVIE_COUNT_SCORE = 1
    __tablename__ = 'artresses'
    id = Column(String(255), primary_key=True)
    name = Column(String(255))
    avatarUrl = Column(String(255))
    star = Column(Boolean, default=False)
    movies = relationship('Movie', secondary=movie_artress, back_populates='artresses',
                          collection_class=set, lazy=True)

    movie_cnt = column_property(select([func.count(movie_artress.c.movie_id)])\
                                .where(movie_artress.c.artress_id == id)\
                                .correlate_except(Movie), deferred=True)

    score = Column(Integer)

    def update_score(self):
        self.score = self._calc_score()

    def _calc_score(self):
        ret = 0
        if self.star:
            ret += self._STAR_SCORE
        for m in self.movies:
            if len(m.artresses) == 1:
                ret += self._MONOPOLIZE_SCORE
            else:
                ret += self._MOVIE_COUNT_SCORE
            if m.series and m.series.star:
                ret += self._STAR_SERIES_SCORE
        return ret

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'avatarUrl': self.avatarUrl,
            'star': self.star,
            # 'movie_cnt': self.movie_cnt,
            # 'movies': [m.id for m in self.movies],
            # 'single_work_cnt': self.single_work_cnt,
        }

    def __repr__(self):
        return u"Artress(name='{}', avatarUrl='{}')".format(self.name, self.avatarUrl)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


Movie.artresses_cnt = column_property(select([func.count(movie_artress.c.artress_id)])
                                      .where(movie_artress.c.movie_id == Movie.id)
                                      .correlate_except(Artress), deferred=True)
Artress.single_work_cnt = column_property(select([func.count(movie_artress.c.movie_id)])
                                          .where(and_(movie_artress.c.artress_id == Artress.id, Movie.artresses_cnt == 1))
                                          .correlate_except(Movie), deferred=True)


class Series(Base):
    __tablename__ = 'serieses'
    name = Column(String(255), primary_key=True)
    movies = relationship('Movie', back_populates='series', collection_class=set)
    star = Column(Boolean, default=False)
    movie_cnt = column_property(select([func.count(Movie.id)])\
                                .where(Movie.series_name == name)\
                                .correlate_except(Movie))

    def __repr__(self):
        return "Series(name='{}')".format(self.name)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


def get_or_new_series(name, session):
    ret = session.query(Series).get(name)
    if not ret:
        ret = Series(name=name)
    return ret


def get_or_new_magnet(hash, url, session, title=None, size=None):
    ret = session.query(MagnetInfo).get(hash)
    if not ret:
        ret = MagnetInfo(hash=hash, url=url)
        if title:
            ret.title = title
        if size:
            ret.size = size
    return ret


class Genre(Base):
    __tablename__ = 'genre'
    name = Column(String(255), primary_key=True)
    movies = relationship('Movie', secondary=movie_genre, back_populates='genres', lazy='dynamic', collection_class=set)
    star = Column(Boolean, default=False)

    def __repr__(self):
        return "Genre(name='{}')".format(self.name)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


class ImageData(Base):
    __tablename__ = 'image_data'
    id = Column(String(255), primary_key=True)
    url = Column(Text)
    headers = Column(Text)
    data = Column(LargeBinary)

    @staticmethod
    def id_from_url(url):
        import hashlib
        return hashlib.md5(url).hexdigest()

    def __init__(self, url, data, headers={}):
        self.id = ImageData.idFromUrl(url)
        self.data = data
        self.headers = repr(headers)

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return self.id

    def __str__(self):
        return self.url


def get_or_new_genre(name, session):
    ret = session.query(Genre).get(name)
    if not ret:
        ret = Genre(name=name)
    return ret


def artresses_order_by_movie_cnt_all(session):
    ret = session.query(Artress).order_by(desc(Artress.movie_cnt)).all()
    return ret


def artresses_cnt(session):
    ret = session.query(Artress).count()
    return ret


def artresses_order_by_movie_cnt(session, start, count):
    ret = session.query(Artress).order_by(desc(Artress.movie_cnt)).slice(start, count)
    return ret


def artresses_order_by_score(session, start, count):
    ret = session.query(Artress).order_by(desc(Artress.score)).slice(start, count)
    return ret


def movie_by_id(session, id):
    ret = session.query(Movie).get(id)
    return ret


def recent_movies(session, count, censored=True):
    query = session.query(Movie)
    query = query.filter(Movie.censored == censored)
    query = query.order_by(desc(Movie.releaseDate))
    ret = query.limit(count)
    return ret


def active_artresses(session, count, censored=True):
    query = session.query(Artress).join(Artress.movies)
    query.filter(Movie.censored==censored)
    query.order_by(desc(Movie.releaseDate))
    return query.limit(count)


def top_score_movies(session, count):
    query = session.query(Movie).order_by(desc(Movie.score))
    return query.limit(count)


if __name__ == '__main__':
    from sqlalchemy import create_engine
    engine = create_engine('mysql://root@localhost/jav')
    from sqlalchemy.orm import sessionmaker
    Session = sessionmaker(bind=engine)
    session = Session()
    Base.metadata.create_all(engine)
