#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models import redismgr
import pickle


def load_cache(key):
    redis = redismgr.connection
    s = redis.get(key)
    if s:
        return pickle.loads(s)
    return None


def save_cache(key, obj):
    redis = redismgr.connection
    s = pickle.dumps(obj)
    redis.set(key, s)