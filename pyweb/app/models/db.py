#!/usr/bin/env python
# -*- coding: utf-8 -*-


from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


_engine = None
_session = None
_Session = None

def setup(conn_str):
    global _engine
    global _Session
    _engine = create_engine(conn_str)
    _Session = sessionmaker(bind=_engine)


def engine():
    assert _engine
    return _engine


def new_session():
    return _Session()
    # Base.metadata.create_all(engine)
