#!/usr/bin/env python
# -*- coding: utf-8 -*-


from flask import Flask
import flask
import requests
import hashlib
from models import redismgr
from models import cachectl
from models import model
from models import db
from flask import Response
import config
from flask import stream_with_context
from flask import g
import flask_restful


import sys
reload(sys)
sys.setdefaultencoding("utf-8")


def create_app():
    if config.debug:
        import logging
        logging.basicConfig()
        logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

    db.setup(config.db_conn_str)
    app = Flask(__name__)
    redismgr.connection_setup(host=config.redis_host, port=config.redis_port)
    app.debug = config.debug
    # app.run(host='0.0.0.0')

    return app




app = create_app()
api = flask_restful.Api(app)


def get_db():
    if not hasattr(g, 'db_session'):
        g.db_session = db.new_session()
    return g.db_session


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'db_session'):
        try:
            if error:
                g.db_session.rollback()
            else:
                g.db_session.commit()
        finally:
            g.db_session.close()


def __date_cmp(date1, date2):
    if date1 is None:
        if date2 is None:
            return 0
        return -1
    if date2 is None:
        return 1
    if date1 > date2:
        return 1
    elif date1 == date2:
        return 0
    else:
        return -1


@app.route("/artresses_all")
def artresses_all():
    # print json.dumps(list(artress.all_artresses()))
    # return json.dumps(list(artress.all_artresses()))
    artresses = model.artresses_order_by_movie_cnt(get_db())
    # print artress[0]

    return flask.render_template("artresses.html",
                                 artresses=artresses)


@app.route("/artresses/<int:page>")
def artresses(page=1):
    print "getting artress page %d" % (page, )
    cnt_per_page = 20
    total_pages = (model.artresses_cnt(get_db()) + 1)/20 + 1
    if page > total_pages:
        page = total_pages
    if page < 1:
        page = 1
    start = (page - 1) * cnt_per_page
    end = start + cnt_per_page
    print "getting artress page %d from %d-%d (%d total)" % (page, start, cnt_per_page + start, total_pages)
    artresses = model.artresses_order_by_score(get_db(), start, end)
    return flask.render_template("artress_list.html", artresses=artresses, total_pages=total_pages, current_page=page)


@app.route("/artress/<id>")
def artress(id):
    return flask.render_template("artress.html",
                                 artress=get_db().query(model.Artress).get(id))


@app.route("/movie/<id>")
def movie_detail(id):
    movie = model.movie_by_id(get_db(), id)
    if not movie:
        flask.abort(404)
    return flask.render_template("movie_detail.html", movie=movie)


@app.route("/series/<name>")
def series(name):
    series = get_db().query(model.Series).get(name)
    return flask.render_template("series_detail.html", series=series)


@app.route("/")
def dashboard():
    recent_movies_censored = model.recent_movies(get_db(), 10, censored=True)
    recent_movies_uncensored = model.recent_movies(get_db(), 10, censored=False)
    active_artress_censored = model.active_artresses(get_db(), 10, censored=True)
    active_artress_uncensored = model.active_artresses(get_db(), 10, censored=False)
    top_score_movies = model.top_score_movies(get_db(), 40)
    return flask.render_template("dashboard.html", top_score_movies=top_score_movies,
                                 recent_movies_censored=recent_movies_censored,
                                 recent_movies_uncensored=recent_movies_uncensored,
                                 active_artress_censored=active_artress_censored,
                                 active_artress_uncensored=active_artress_uncensored)


@app.route("/api/")
def api_root():
    flask.abort(404)


# @app.route("/api/star/artress/<id>")
# def star_artress(id):
#     if flask.request.method == "GET":
#         artress = get_db().query(model.Artress).get(id)
#         if not artress:
#             flask.abort(404)
#         return flask.jsonify(id=id, is_stared=artress.star)
#     elif flask.request.method == "POST":
#         artress = get_db().query(model.Artress).get(id)
#         if artress and not artress.star:
#             artress.star = True
#             get_db().commit()
#             return


class Artress(flask_restful.Resource):
    def __init__(self):
        from flask_restful import reqparse
        self.__parser = reqparse.RequestParser()
        self.__parser.add_argument("star", type=int)

    def get(self, artress_id):
        artress = get_db().query(model.Artress).get(artress_id)
        if not artress:
            flask_restful.abort(404, message="Artress {} not exist.".format(artress_id))
        return artress.to_dict()

    def put(self, artress_id):
        artress = get_db().query(model.Artress).get(artress_id)
        if not artress:
            flask_restful.abort(404, message="Artress {} not exist.".format(artress_id))
        args = self.__parser.parse_args(strict=True)
        print "Updating artress '{}'".format(repr(args))
        artress.star = args.star == 1
        artress.update_score()
        get_db().commit()
        return artress.to_dict()


class Movie(flask_restful.Resource):

    def get(self, movie_id):
        movie = get_db().query(model.Movie).get(movie_id)
        if not movie:
            flask_restful.abort(404, message="Movie {} not found".format(movie_id))
        return movie.to_dict()


class MovieList(flask_restful.Resource):
    def __init__(self):
        from flask_restful import reqparse
        self.__parser = reqparse.RequestParser()
        self.__parser.add_argument("star", type=int)

    def get(self):
        args = self.__parser.parse_args()
        movies = []
        if args.star is not None:
            movies = get_db().query(model.Movie).join(model.Movie.artresses) \
                        .filter(model.Artress.star == 1).all()
        else:
            movies = get_db().query(model.Movie).order_by(sqlalchemy.desc(model.Movie.releaseDate))
        return [m.to_dict() for m in movies]


api.add_resource(Artress, '/api/artress/<artress_id>', endpoint='api_artress')
api.add_resource(Movie, '/api/movie/<movie_id>', endpoint='api_movie')
api.add_resource(MovieList, '/api/movie', endpoint='api_movies')


@app.route("/testpath")
def testpath():
    return flask.redirect(flask.url_for("login"))


@app.route("/login")
def login():
    return "This is login"


@app.route("/getimage")
def image_proxy():
    url = flask.request.args.get('url', None)
    if not url:
        url = 'http://docs.python-requests.org/en/latest/_static/requests-sidebar.png'
    return flask.redirect("/lua_proxy?url=%s" % url)


@app.template_filter('format_time')
def _filter_format_time(date, fmt=None):
    import time
    if not fmt:
        fmt = "%Y-%m-%d"
    return time.strftime(fmt, date)


def main():
    app.run(host='0.0.0.0')


if __name__ == '__main__':
    main()
