#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


import argparse

parser = argparse.ArgumentParser(description='Bootstrap script for pyweb')
subparser = parser.add_subparsers()

initdb_parser = subparser.add_parser('initdb')
initdb_parser.set_defaults(action='initdb')
initdb_parser.add_argument('--rebuild', action='store_true', default=False)

webserver_parser = subparser.add_parser('webserver')
webserver_parser.set_defaults(action='webserver')

test_data_parser = subparser.add_parser('test_data')
test_data_parser.set_defaults(action='test_data')

test_data_parser = subparser.add_parser('renew_score')
test_data_parser.set_defaults(action='renew_score')


#
# parser.add_argument('action')

args = parser.parse_args()

print args

if args.action == 'initdb':
    import config
    import MySQLdb
    print {'host':config.db_host, 'user': config.db_user,
                      'passwd': config.db_password, 'port': config.db_port}
    db = MySQLdb.connect(host=config.db_host, user=config.db_user,
                      passwd=config.db_password, port=config.db_port)
    c = db.cursor()
    if args.rebuild:
        c.execute("""DROP DATABASE IF EXISTS `%s`;""" % (config.db_database, ))
    c.execute("""CREATE DATABASE /*!32312 IF NOT EXISTS*/ `%s` /*!40100 DEFAULT CHARACTER SET utf8 */;""" % (config.db_database,))
    db.close()
    from models import model
    from sqlalchemy import create_engine
    engine = create_engine(config.db_conn_str)
    model.Base.metadata.create_all(engine)
elif args.action == 'webserver':
    import server
    import config
    server.main()
elif args.action == 'test_data':
    from models import model, db
    import config, datetime
    db.setup(config.db_conn_str)
    session = db.new_session()
    series_even = model.Series()
    series_even.name = "Even Series"
    series_odd = model.Series()
    series_odd.name = 'Odd Series'
    genres = []
    for g_id in range(0, 8):
        genre = model.Genre()
        genre.name = "Genre{}".format(g_id)
        genres.append(genre)
    genre_all = model.Genre()
    genre_all.name = "Genre All"
    for a_id in range(0, 10):
        artress = model.Artress()
        artress.id = "artress_{}".format(a_id)
        artress.avatarUrl = 'http://memesvault.com/wp-content/uploads/Doge-Meme-05.png'
        artress.name = "No.{} artress".format(a_id)
        session.add(artress)
        for m_id in range(0, 8):
            movie = model.Movie()
            movie.id = "movie_{}_{}".format(a_id, m_id)
            movie.title = "No.{} movie".format(m_id)
            movie.censored = m_id % 2 == 0
            movie.coverUrl = 'http://memesvault.com/wp-content/uploads/Doge-Meme-01.jpg'
            movie.updatedDate = datetime.datetime.now()
            movie.artresses.add(artress)
            if m_id not in (4, 7):
                if m_id % 2 == 0:
                    movie.series = series_even
                else:
                    movie.series = series_odd
            movie.genres.add(genres[m_id])
            movie.genres.add(genre_all)
    session.commit()
elif args.action == 'renew_score':
    import calc_score
    calc_score.main()



