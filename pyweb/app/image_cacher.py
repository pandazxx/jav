#!/usr/bin/env python
# -*- coding: utf-8 -*-


import hashlib
import requests
import sys
import models.redismgr as redismgr
import models.movie as movie
import models.cachectl as cachectl
import models.artress as artress


def cache_image(redis, url):
    cache_key = "imgcache:" + hashlib.sha1(url).hexdigest()
    print "Trying to cache image %s with hash %s" % (url, cache_key)
    if redis.exists(cache_key):
        print "Cache exists: %s(%s)" % (url, cache_key)
        return
    try:
        resp = requests.get(url)
        if resp.status_code != 200:
            print "Fetching image %s error, code %s" % (url, resp.status_code)
            return
    except Exception as e:
        print "Fetching image %s error, error: %s" % (url, e)
        return
    cachectl.save_cache(cache_key,
                        (resp.content,
                         resp.status_code,
                         resp.headers.items()))
    print "Cache saved %s(%s)" % (url, cache_key)


def main():
    if len(sys.argv) < 3:
        print "Usage %s redis_host redis_port" % sys.argv[0]
        return
    redis_host = sys.argv[1]
    redis_port = sys.argv[2]
    redismgr.connection_setup(host=redis_host, port=redis_port)
    # r = redis.StrictRedis(host=redis_host, port=redis_port, db=0)
    r = redismgr.connection
    for movie_id in r.sscan_iter("movies", count=10):
        m = movie.get_movie(movie_id)
        url = m.coverUrl
        cache_image(r, url)
    for artress_id in r.sscan_iter("artists", count=10):
        art = artress.get_artress(artress_id)
        url = art.avatarUrl
        cache_image(r, url)


if __name__ == '__main__':
    main()
