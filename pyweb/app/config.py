#!/usr/bin/env python
# -*- coding: utf-8 -*-


db_user = 'root'
db_password = ''
db_host = 'jav-mysql'
db_port = 3306
db_database = 'jav'
db_options = '?charset=utf8'


debug = True


db_conn_str = 'mysql://%s:%s@%s:%d/%s%s' % (db_user, db_password, db_host,db_port, db_database, db_options)

redis_host = 'jav-redis'
redis_port = 6379