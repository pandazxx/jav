#!/usr/bin/env bash

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LC_CTYPE=UTF-8

if [ $# -lt 1 ]; then
    echo "Usage `basename $0` action [arguments]"
    exit 1
fi

action=$1
shift

case "$action" in
    crawl_all)
        for spider in `scrapy list`; do
            scrapy crawl "$spider" "$@"
        done
    ;;
    crawl)
        scrapy crawl "$@"
    ;;
    uwsgi)
        uwsgi --ini wsgi.ini
    ;;
    *)
        ./bootstrap.py "$action" "$@"
    ;;
esac
