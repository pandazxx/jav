#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

SINGLE_ARTRESS_STAR_SCORE = 500
ARTRESS_STAR_SCORE = 50
SERIES_STAR_SCORE = 300
GENRE_STAR_SCORE = 25

def renew(movie):
    score = 0
    if movie.artresses_cnt == 1:
        for a in movie.artresses:
            score += SINGLE_ARTRESS_STAR_SCORE if a.star else 0
    else:
        for artress in movie.artresses:
            if artress.star:
                score += ARTRESS_STAR_SCORE
    if movie.series and movie.series.star:
        score += SERIES_STAR_SCORE
    for g in movie.genres:
        if g.star:
            score += GENRE_STAR_SCORE
    movie.score = score


def renew_all(movies):
    for m in movies:
        renew(m)

def renew_all_artresses(artresses):
    for a in artresses:
        a.update_score()


def main():
    from models import model, db
    import config
    db.setup(config.db_conn_str)
    session = db.new_session()
    # for movie in session.query(model.Movie).yield_per(100):
    #     renew(movie)
    cnt = 0
    for artress in session.query(model.Artress).yield_per(100):
        artress.update_score()
        print "Updated {} score to {}".format(artress.name, artress.score)
        cnt += 1
        if cnt % 10 == 0:
            session.commit()
    session.commit()


if __name__ == "__main__":
    main()