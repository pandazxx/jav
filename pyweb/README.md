## Docker Usage

### mysql
```
docker run -d --name mysql -p 13306:3306 -e MYSQL_ROOT_PASSWORD=abc123 mysql
```

### sockstore
```
docker build -t sock_store .
docker create --name sock_store sock_store true
```

### redis
```
docker build -t jav/redis .
docker run --volumes-from sock_store -d --name jav_redis jav/redis
```

### nginx
```
docker build -t jav/nginx .
docker run -d --name jav_nginx -p 8080:8080 --volumes-from sock_store jav/nginx
```


### elasticsearch
```
docker build -t elasticsearch .
docker run -d --name elasticsearch -p 19200:9200 elasticsearch gosu elasticsearch /es_root/bin/elasticsearch
```